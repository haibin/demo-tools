# Demo tools

[![Build Status](https://gitlab.com/haibin/demo-tools/badges/master/build.svg)](https://gitlab.com/haibin/demo-tools/commits/master) [![Coverage Report](https://gitlab.com/haibin/demo-tools/badges/master/coverage.svg)](https://gitlab.com/haibin/demo-tools/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/haibin/demo-tools)](https://goreportcard.com/report/gitlab.com/haibin/demo-tools) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

## What is it?

demo-tools is a repository for the code used in the blog post https://medium.com/pantomath/golang-tools-on-gitlab-941a3a9ad0b6